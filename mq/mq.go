package mq

import (
	"gitlab.com/nick25214/go-mq/glob"
	"gitlab.com/nick25214/go-mq/message"

	stringutil "gitlab.com/nick25214/nick-lib/util/string"
)

type MQType string

const (
	MQTypeRedis MQType = "ps.micro.service.redismq"
	MQTypeTest  MQType = "ps.micro.service.testmq"
)

type MQServer struct {
	ServerCfg interface{}
	MQEnginge MQType
	MQSrc     *string
	MQDest    *string
}

type MQ interface {
	connect(server MQServer) (MQ, error)
	Start() error
	Close() error
	IsStart() bool
	IsClose() bool
	Send(msg *message.MQMessage) error
	Receive() (*message.MQMessage, error)
	GetMQSettings() (MQServer, error)
	SrcLen() int64
	DestLen() int64
	SetSource(name string) error
	SetDestination(name string) error
	Ack() error
}

func CreateMQ() (MQ, error) {
	var q MQ
	engine := loadQueueEngine()
	if engine == MQTypeRedis {
		q = &RedisMQ{}
	}

	if engine == MQTypeTest {
		q = &LocalMQ{}
	}

	queue, err := q.connect(loadServerConfig(engine))
	return queue, err
}

func loadQueueEngine() MQType {
	c := glob.LoadConfig()
	eng := c.GetValue("agent", "engine", "")
	if eng == "redis" {
		return MQTypeRedis
	}

	return MQTypeRedis
}

func loadServerConfig(t MQType) MQServer {
	server := &MQServer{}
	if t == MQTypeRedis {
		loadRedisConfig(server)
	}

	return *server
}

func loadRedisConfig(server *MQServer) {
	c := glob.LoadConfig()
	server.ServerCfg = RedisServer{
		Password:       c.GetValue("redis", "password", ""),
		Server:         c.GetValue("redis", "server", ""),
		Pool:           stringutil.Atoi(c.GetValue("redis", "pool", ""), 0),
		ConnectTimeout: stringutil.Atoi(c.GetValue("redis", "timeout_second", "1"), 1),
	}

	src := c.GetValue("redis", "source_queue", "")
	dest := c.GetValue("redis", "destination_queue", "")

	if stringutil.HasValue(src) {
		server.MQSrc = &src
	}

	if stringutil.HasValue(dest) {
		server.MQDest = &dest
	}
	server.MQEnginge = MQTypeRedis
}
