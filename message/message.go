package message

import (
	"time"

	uuid "github.com/satori/go.uuid"
)

type MQMessage struct {
	ID        string
	Message   string
	TimeStamp time.Time
}

func NewMQMessage(message string) *MQMessage {
	id, _ := uuid.NewV4()
	return &MQMessage{
		ID:        id.String(),
		TimeStamp: time.Now().UTC(),
		Message:   message,
	}
}
