package glob

import (
	"fmt"

	"gitlab.com/nick25214/nick-lib/config"
)

func LoadConfig() *config.Config {
	c, err := config.Load("app.conf")
	if err != nil {
		panic(fmt.Sprintf("cannot load config: %s", "app.conf"))
	}

	return c
}
